package main

//go:generate protoc -I=proto --go_out=import_path=main:. proto/common.proto  proto/mon_msg.proto

import (
	"flag"
	"fmt"
	"github.com/go-mangos/mangos"
	"github.com/go-mangos/mangos/protocol/sub"
	"github.com/go-mangos/mangos/transport/ipc"
	"github.com/go-mangos/mangos/transport/tcp"
	"github.com/golang/protobuf/proto"
	"os"
)

func die(format string, v ...interface{}) {
	fmt.Fprintln(os.Stderr, fmt.Sprintf(format, v...))
	os.Exit(1)
}

func main() {

	var url = flag.String("url", "tcp://localhost:5555", "URL for socket")
	flag.Parse()

	var sock mangos.Socket
	var err error
	var data []byte

	if sock, err = sub.NewSocket(); err != nil {
		die("can't get new sub socket: %s", err.Error())
	}

	sock.AddTransport(ipc.NewTransport())
	sock.AddTransport(tcp.NewTransport())
	if err = sock.Dial(*url); err != nil {
		die("can't dial on sub socket: %s", err.Error())
	}
	// Empty byte array effectively subscribes to everything
	err = sock.SetOption(mangos.OptionSubscribe, []byte(""))
	if err != nil {
		die("cannot subscribe: %s", err.Error())
	}

	fmt.Printf("Nanocat listening to %s\n", *url)
	for {
		data, err = sock.Recv()
		if err != nil {
			die("Cannot recv: %s", err.Error())
		}

		msg := &MonitoringEvt{}
		err = proto.Unmarshal(data, msg)
		if err != nil {
			fmt.Printf("RECEIVED %s\n", string(data))
		} else {
			fmt.Printf("RECEIVED %v\n", string(msg.String()))

		}

	}
}
